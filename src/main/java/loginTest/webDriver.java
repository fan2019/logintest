package loginTest;

import org.openqa.selenium.By;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.WebDriver;

import java.util.concurrent.TimeUnit;

public class webDriver {

    public static WebDriver driver;

    public static void main(String[] args) {

        System.setProperty("webdriver.chrome.driver","/Users/yangfan/Downloads/chromedriver");

        driver = new ChromeDriver();

        // driver to navigate any URL

        driver.navigate().to("https://www.amazon.co.uk/ap/signin?openid.pape.max_auth_age=0&openid.return_to=https%3A%2F%2Fwww.amazon.co.uk%2F%3Fref_%3Dnav_ya_signin&openid.identity=http%3A%2F%2Fspecs.openid.net%2Fauth%2F2.0%2Fidentifier_select&openid.assoc_handle=gbflex&openid.mode=checkid_setup&openid.claimed_id=http%3A%2F%2Fspecs.openid.net%2Fauth%2F2.0%2Fidentifier_select&openid.ns=http%3A%2F%2Fspecs.openid.net%2Fauth%2F2.0&");

        // how many second need to wait the elements to load
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);

        driver.findElement(By.cssSelector("#signInSubmit")).click();

        driver.findElement(By.cssSelector("#ap_email")).sendKeys("2359448y@student.gla.ac.uk");

        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);

        driver.findElement(By.cssSelector("#ap_password")).sendKeys("yf19921215");

        System.out.println("on password page, "+driver.getCurrentUrl());

        System.out.println("on password page, "+driver.getTitle());

        System.out.println("on password page, "+driver.getWindowHandle());

        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        // can identify successful login or not
        System.out.println("successful login, "+driver.getCurrentUrl());

        System.out.println("successful login, "+driver.getTitle());

        System.out.println("successful login, "+driver.getWindowHandle());

        // if we use the driver at the end we need close the driver
        driver.close();
        driver.quit();

    }

    public void openLoginPage(){

        System.setProperty("webdriver.chrome.driver","/Users/yangfan/Downloads/chromedriver");
        driver = new ChromeDriver();
        // driver to navigate any URL
        driver.navigate().to("https://www.amazon.co.uk/ap/signin?openid.pape.max_auth_age=0&openid.return_to=https%3A%2F%2Fwww.amazon.co.uk%2F%3Fref_%3Dnav_ya_signin&openid.identity=http%3A%2F%2Fspecs.openid.net%2Fauth%2F2.0%2Fidentifier_select&openid.assoc_handle=gbflex&openid.mode=checkid_setup&openid.claimed_id=http%3A%2F%2Fspecs.openid.net%2Fauth%2F2.0%2Fidentifier_select&openid.ns=http%3A%2F%2Fspecs.openid.net%2Fauth%2F2.0&");
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);

    }

    public void enterUsername(String username){

        driver.findElement(By.cssSelector("#ap_email")).sendKeys(username);
        // how many second need to wait the elements to load
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
    }

    public void enterPassword(String password){

        driver.findElement(By.cssSelector("#ap_password")).sendKeys(password);
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);

    }

    public  void clickLoginButton(){
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        driver.findElement(By.cssSelector("#signInSubmit")).click();

    }

    public boolean isSuccessfulLogin(){

        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println("successful login, "+driver.getCurrentUrl());

        if (driver.getCurrentUrl().equals("https://www.amazon.co.uk/?ref_=nav_ya_signin&")){
            return true;
        }else{

            return false;
        }

    }

    public void closeAndQuit(){
        // if we use the driver at the end we need close the driver
        driver.close();
        driver.quit();
    }
}
