package loginTest;

import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Named;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.junit.Assert;
import loginTest.webDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import javax.sql.rowset.spi.SyncResolver;

public class login {

    private webDriver driver = new webDriver();

    private boolean result;

    @Given("open the login page")
    public void givenOpenTheLoginPage() {
        driver.openLoginPage();
    }

    @When("I enter the username as <username>")
    public void whenIEnterTheUsernameAsusername(@Named("username") String username) {
        driver.enterUsername(username);
    }

    @When("I enter the password as <password>")
    public void whenIEnterThePasswordAspassword(@Named("password") String password) {
        driver.enterPassword(password);
    }

    @When("I click login button")
    public void whenIClickLoginButton() {
        driver.clickLoginButton();
    }

    @Then("I should login successful as <result>")
    public void thenIShouldLoginSuccessfulAsresult(@Named("result") boolean result) {
        this.result = driver.isSuccessfulLogin();
        System.out.println(this.result);
        System.out.println(result);
        Assert.assertSame(this.result,result);
        driver.closeAndQuit();
    }

}
