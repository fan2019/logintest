
Scenario: scenario frist enter username and last click the login button
Given open the login page
When I enter the username as <username>
And I enter the password as <password>
And I click login button
Then I should login successful as <result>

Examples:
|username|password|result|
|2359448y@student.gla.ac.uk|yf19921215|true|

Scenario: scenario frist enter password and last click the login button
Given open the login page
When I enter the password as <password>
And I enter the username as <username>
And I click login button
Then I should login successful as <result>

Examples:
|password|username|result|
|yf19921215|2359448y@student.gla.ac.uk|true|

Scenario: scenario frist enter username then click the login button and last enter password
Given open the login page
When I enter the username as <username>
And I click login button
And I enter the password as <password>
Then I should login successful as <result>

Examples:
|username|password|result|
|2359448y@student.gla.ac.uk|yf19921215|false|

Scenario: scenario click the login button then enter username and password
Given open the login page
When I click login button
And I enter the username as <username>
And I enter the password as <password>
Then I should login successful as <result>

Examples:
|username|password|result|
|yf19921215|2359448y@student.gla.ac.uk|false|

